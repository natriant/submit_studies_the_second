from studies import config


class Mask(object):
    def __init__(self):
        self.status_type = None
        self.dpp = None
        self.qxp = None
        self.qyp = None
        self.energy = None
        self.prefix = None
        self.optics = None
        """dependent values"""
        self.z = None

    def get_vrf(self):
        """ energy and status --> change the voltage of the RF """
        if self.energy == 26000. and self.status_type == '6D':
            return 2
        elif self.status_type == '6D':
            return 5

        return 0

    @staticmethod
    def build_from_conf():
        mask = Mask()

        mask.status_type = config.data['status_type']
        mask.dpp = config.data['dpp']
        mask.qxp = config.data['Qxp']
        mask.qyp = config.data['Qyp']
        mask.energy = config.data['energy']
        mask.optics = config.data['optics']
        mask.prefix = config.data['mask_prefix']

        return mask
