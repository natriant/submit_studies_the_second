from studies.entity import CrabCavity


class CrabCavityMask(object):

    def __init__(self):
        self.v1 = None
        self.v2 = None
        self.lag1 = None
        self.lag2 = None
        self.tilt1 = None
        self.tilt2 = None
        self.dynk = None

        self.voltage = None

    @staticmethod
    def build_from_cc(
            cc,  # type: CrabCavity
            cc_voltage
            ):
        cc_mask = CrabCavityMask()

        cc_mask.voltage = cc_voltage
        if cc_voltage > 0:
            cc_mask.v1 = str(cc_voltage)
            cc_mask.v2 = str(cc_voltage + 0.000001)
        else:
            cc_mask.v1 = 0
            cc_mask.v2 = 0
            cc_mask.voltage = '0.0'

        cc_mask.lag1 = cc.phase
        cc_mask.lag2 = cc.phase

        if cc.orientation == 'V':
            cc_mask.tilt1 = 'PI/2.'
            cc_mask.tilt2 = 'PI/2.'
        else:
            cc_mask.tilt1 = '0.0'
            cc_mask.tilt2 = '0.0'

        cc_mask.dynk = cc.dynk

        return cc_mask
