from studies import config


class Study(object):

    def __init__(self):
        self.type = None

    @staticmethod
    def build_from_conf():
        study = Study()
        study.type = config.data['type_of_study']
        return study
