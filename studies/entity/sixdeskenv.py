class Sixdeskenv(object):

    def __init__(self):
        self.optics = None
        self.energy = None
        self.Qxp = None
        self.Qyp = None
        self.dpp = None
        self.status_type = None
        self.type_of_input = None
        self.min_amplitude = None
        self.max_amplitude = None
        self.limit_amplitude = None
        self.min_angle = None
        self.max_angle = None
        self.limit_angle = None
        self.fort13 = None
        self.turns = None
        self.tue = None

    def get_gamma(self):
        return float(self.energy)/938.272

    def calculate_type_of_study(self, turns, tue,
                                min_amplitude, max_amplitude, limit_amplitude,
                                min_angle, max_angle, limit_angle):
        self.turns = str(turns)
        self.tue = tue
        if self.type_of_input == 'sixdesk':
            self.min_amplitude = str(min_amplitude)
            self.max_amplitude = str(max_amplitude)
            self.limit_amplitude = str(limit_amplitude)
            self.min_angle = str(min_angle)
            self.max_angle = str(max_angle)
            self.limit_angle = str(limit_angle)
            self.fort13 = '0'
        else:
            self.fort13 = '1'

    @staticmethod
    def build_from_conf():
        from studies import config

        sixdeskenv = Sixdeskenv()

        sixdeskenv.optics = str(config.data['optics'])
        sixdeskenv.energy = str(config.data['energy'])
        sixdeskenv.Qxp = str(config.data['Qxp'])
        sixdeskenv.Qyp = str(config.data['Qyp'])
        sixdeskenv.dpp = str(config.data['dpp'])
        sixdeskenv.status_type = config.data['status_type']
        sixdeskenv.type_of_input = config.data['type_of_input']

        study_type = config.data['type_of_study']
        if study_type == 'fma':
            sixdeskenv.calculate_type_of_study(10000, '4', 1, 15, 1, 1, 99, 99)
        elif study_type == 'da':
            sixdeskenv.calculate_type_of_study(1000000, '6', 2, 99, 2, 1, 9, 9)
        # TODO: else raise exception study type not supported

        return sixdeskenv
