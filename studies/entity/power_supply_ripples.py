class PowerSupplyRipples(object):

    def __init__(self):
        self.status = None
        self.amplitude = None
        self.phase = None
        self.frequency_min = None
        self.frequency_max = None
        self.frequency_step = None

    @staticmethod
    def build_from_config():
        from studies import config
        ripple = PowerSupplyRipples()
        ripple.status = config.data['power_supply_ripples']['status']
        ripple.amplitude = config.data['power_supply_ripples']['amplitude']
        ripple.frequency_min = config.data['power_supply_ripples']['freq_limits']['min']
        ripple.frequency_max = config.data['power_supply_ripples']['freq_limits']['max']
        ripple.frequency_step = config.data['power_supply_ripples']['freq_limits']['step']
        ripple.phase = config.data['power_supply_ripples']['phase']
        return ripple
