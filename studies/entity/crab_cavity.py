class CrabCavity(object):

    def __init__(self):
        self.v_min = None
        self.v_max = None
        self.step = None
        self.phase = None
        self.orientation = None
        self.dynk = None

    @staticmethod
    def build_from_config():
        from studies import config
        cc = CrabCavity()
        cc.v_min = config.data['crab_cavities']['Vmin']
        cc.v_max = config.data['crab_cavities']['Vmax']
        cc.step = config.data['crab_cavities']['step']
        cc.phase = config.data['crab_cavities']['phase']
        cc.orientation = config.data['crab_cavities']['orientation']
        cc.dynk = config.data['crab_cavities']['DYNK']
        return cc
