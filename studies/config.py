import json

data = {}
workspace = None


def init_data(conf_file="./config.json"):
    global data
    with open(conf_file, "r") as read_file:
        data = json.load(read_file)


def is_crab_cavities():
    global data
    return data['crab_cavities']['Vmax'] > 0


def is_power_supply_ripples():
    global data
    return data['power_supply_ripples']['status']
