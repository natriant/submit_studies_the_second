from shutil import copyfile

from studies import config
from studies.service import ReplaceService


def setup_fort3():
    study_type = config.data['type_of_study']

    if study_type == 'da':
        block_name = '/'  # TODO: Attention, similar to 'else'
    elif study_type == 'fma':
        block_name = 'DUMP \nip3..1 1 1030 2 IP3_DUMP_1 1 2000 \nip3..2 1 1032 2 IP3_DUMP_2 8000 10000 \nNEXT\nLINE\nELEMENT 0 2 1 2.5 2.5\nNEXT\nFMA\nIP3_DUMP_1 NAFF\nIP3_DUMP_2 NAFF\nNEXT\nZIPF\nfma_sixtrack\nNEXT'
    else:
        block_name = ''

    ReplaceService.batch_replace({'%DAFMA': block_name},
                                 './templates/fort.3.local.template',
                                 './generated/fort.3.local.template.temp')


def setup_mask():
    from studies.entity import Mask
    mask = Mask.build_from_conf()

    mask_dictionary = {'%CUR_WORK': config.workspace,
                       '%VRF': str(mask.get_vrf()),
                       '%STATUS': mask.status_type,
                       '%OPTICS': str(mask.optics),
                       '%QXP': str(mask.qxp),
                       '%QYP': str(mask.qyp)}

    ReplaceService.batch_replace(mask_dictionary,
                                 './templates/SPStemplate_MV_MULT_D_z_dpp_Qxp_Qyp_optics_deg.mask',
                                 './generated/SPStemplate_MV_MULT_D_z_dpp_Qxp_Qyp_optics_deg.mask.temp')


def setup_sixdeskenv():
    from studies.entity import Sixdeskenv
    sixdeskenv = Sixdeskenv.build_from_conf()
    sixdeskenv_dictionary = {'%CUR_WORK': config.workspace,
                             '%DPP': sixdeskenv.dpp,
                             '%QXP': sixdeskenv.Qxp,
                             '%QYP': sixdeskenv.Qyp,
                             '%GAMMA': str(sixdeskenv.get_gamma()),
                             '%ENERGY': sixdeskenv.energy,
                             '%TURNS': sixdeskenv.turns,
                             '%TUE': sixdeskenv.tue,
                             '%MIN_AMPL': sixdeskenv.min_amplitude,
                             '%MAX_AMPL': sixdeskenv.max_amplitude,
                             '%LIM_AMPL': sixdeskenv.limit_amplitude,
                             '%MIN_ANGLE': sixdeskenv.min_angle,
                             '%MAX_ANGLE': sixdeskenv.max_angle,
                             '%LIM_ANGLE': sixdeskenv.limit_angle,
                             '%FORT_13': sixdeskenv.fort13,
                             '%OPTICS': sixdeskenv.optics}

    ReplaceService.batch_replace(sixdeskenv_dictionary,
                                 './templates/sixdeskenv.template',
                                 './generated/sixdeskenv.template.temp')


def setup_mask_sixdeskenv(mask_name):
    ReplaceService.batch_replace({'%MASK': mask_name},
                                 './generated/sixdeskenv.template.temp',
                                 './generated/sixdeskenv.{}'.format(mask_name))


def setup_fort3mother(mask, z):
    mother_dictionary = {'%z1': str(z),
                         '%z2': str(z)}
    if mask.status_type == '5D':
        mother_dictionary['%iclo6'] = '1'

    ReplaceService.batch_replace(mother_dictionary,
                                 './templates/fort.3.mother1_inj.template',
                                 './generated/fort.3.mother1_inj')


def create_mask_file(mask, study_name, variable):
    # variable : is either times or random , depends on the study
    mask_name = '{}_{}'.format(mask.prefix, study_name)
    mask_filepath = './generated/' + mask_name + '.mask'
    if not config.data['multipole_errors']['nominal_value']:
        copyfile('./generated/SPStemplate_MV_MULT_times{}_D_z_dpp_Qxp_Qyp_optics_deg.mask.temp'.format(variable),
                 mask_filepath)
    if config.data['multipole_errors']['random_flag']:
        copyfile('./generated/SPStemplate_MV_MULT_random{}_D_z_dpp_Qxp_Qyp_optics_deg.mask.temp'.format(variable), mask_filepath)
    elif config.data['multipole_errors']['nominal_value']:
        copyfile('./generated/SPStemplate_MV_MULT_D_z_dpp_Qxp_Qyp_optics_deg.mask.temp', mask_filepath)
    return mask_name


def create_fort3_file(study_name):
    fort3_name = 'fort.3.local.{}'.format(study_name)
    fort3_filepath = './generated/' + fort3_name
    copyfile('./generated/fort.3.local.template.temp', fort3_filepath)
    return fort3_name


def move_files_to_workspace(fort3_filename, mask_filename):
    common_path = '../{}/scratch0/w1/sixjobs'.format(config.workspace)

    copyfile('./generated/' + fort3_filename,  '{}/{}'.format(common_path, fort3_filename))
    copyfile('./generated/' + fort3_filename,  '{}/fort.3.local'.format(common_path))
    copyfile('./generated/' + mask_filename,   '{}/mask/{}'.format(common_path, mask_filename))
    copyfile('./generated/fort.3.mother1_inj', '{}/control_files/fort.3.mother1_inj'.format(common_path))
    copyfile('./generated/sixdeskenv.{}'.format(mask_filename[:-5]), '{}/sixdeskenv.{}'.format(common_path,mask_filename[:-5]))
