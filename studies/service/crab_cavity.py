import numpy as np

from studies import config
from studies.registry import Registry as Reg
from studies.entity import CrabCavity, CrabCavityMask
from studies.service import ReplaceService, multipole_error


def setup():
    multipole_error.setup()


def setup_mask(mask_template_filepath, cc_mask):
    block = 'CRAVITY.1, VOLT={}, FREQ=400, TILT={}, LAG={};\n'.format(
        str(cc_mask.v1), str(cc_mask.tilt1), str(cc_mask.lag1))
    block += 'CRAVITY.2, VOLT={}, FREQ=400, TILT={}, LAG={};'.format(
        str(cc_mask.v2), str(cc_mask.tilt2), str(cc_mask.lag2))

    cc_mask_dictionary = {'!%CC': block}

    ReplaceService.batch_replace(cc_mask_dictionary, mask_template_filepath)


def setup_fort3(fort3_template_filepath, cc_voltage, dynk):
    block_name = '/'

    if cc_voltage != 0.0:
        block_name = 'DYNK' + '\n' + 'FUN crabVolt1 GET cravity.1 voltage' + '\n' + 'FUN crabVolt2 GET cravity.2 voltage' + '\n' + 'FUN ramp_CC1 LINSEG 0 ' + str(dynk) + ' 0 ' + str(cc_voltage) + '\n' + 'FUN ramp_CC2 LINSEG 0 ' + str(dynk) + ' 0 ' + str(cc_voltage) + '\n' + 'SET cravity.1 voltage ramp_CC1 1 ' + str(dynk) + ' -1' + '\n' + 'SET cravity.2 voltage ramp_CC2 1 ' + str(dynk) + ' -1' + '\n' + 'NEXT'

    ReplaceService.batch_replace({'/%CC': block_name}, fort3_template_filepath)


def get_study_name():
    mask = Reg.get('mask')
    multipole_errors = config.data['multipole_errors']
    study_type = config.data['type_of_study']
    z = Reg.get('z')
    cc_mask = Reg.get('cc_mask')

    return 'VCC{}_{}_{}_Z{}_DPP{}_QXP{}_QYP{}_Q{}_DEG{}_{}_DYNK{}_EN{}_{}'.format(
        str(cc_mask.voltage), multipole_errors, mask.status_type, str(z), str(mask.dpp),
        str(mask.qxp), str(mask.qyp), mask.optics, cc_mask.lag1,
        config.data['crab_cavities']['orientation'], cc_mask.dynk,
        mask.energy, study_type)


def scan_cc():
    cc = CrabCavity.build_from_config()
    cc_range = np.arange(cc.v_min, cc.v_max, cc.step)

    for cc_voltage in cc_range:
        Reg.store('cc_mask', CrabCavityMask.build_from_cc(cc, cc_voltage))
        # next iteration goes to multipole error file, apart from the name
        for z in multipole_error.scan_z():
            cc_mask = Reg.get('cc_mask')
            mask_name = Reg.get('mask_name')
            fort3_name = Reg.get('fort3_name')

            mask_filepath = './generated/' + mask_name + '.mask'
            setup_mask(mask_filepath, cc_mask)
            fort3_filepath = './generated/' + fort3_name
            setup_fort3(fort3_filepath, cc_mask.voltage, cc_mask.dynk)


def do():
    multipole_error.get_study_name = get_study_name

    setup()
    scan_cc()
