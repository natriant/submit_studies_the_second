from __future__ import print_function

import fileinput
import re
from contextlib import closing

from studies import config


class ReplaceService(object):

    @staticmethod
    def __read_file(input_filepath):
        with open(input_filepath) as f:
            lines = f.readlines()
        return lines

    @staticmethod
    def batch_replace(replacements,  # type: dict
                      input_filepath,  # type: str
                      output_filepath = None  # type: str
                      ):
        if not output_filepath:
            ReplaceService.__batch_replace_same_file(replacements, input_filepath)
        else:
            ReplaceService.__batch_replace_new_file(replacements, input_filepath, output_filepath)

    @staticmethod
    def __batch_replace_new_file(replacements,  # type: dict
                                 input_filepath,  # type: str
                                 output_filepath  # type: str
                                 ):
        with open(input_filepath, "rt") as fin:
            with open(output_filepath, "wt") as fout:
                for line in fin:
                    fout.write(ReplaceService.replace_line(line, replacements))

    @staticmethod
    def __batch_replace_same_file(replacements,  # type: dict
                                  input_filepath,  # type: str
                                  ):
        with closing(fileinput.FileInput(input_filepath, inplace=True)) as file:
            for line in file:
                print(ReplaceService.replace_line(line, replacements), end='')

    @staticmethod
    def replace_line(line, replacements):
        replacements = dict((re.escape(k), v) for k, v in replacements.iteritems())
        pattern = re.compile("|".join(replacements.keys()))
        return pattern.sub(lambda m: replacements[re.escape(m.group(0))], line)

    @staticmethod
    def replace_working_directory():
        import os
        for fname in os.listdir('./generated'):
            filepath = './generated/' + fname
            if os.path.isfile(filepath):
                with open(filepath) as f:
                    for line in f:
                        if '%MY_WORK_DIR' in line:
                            ReplaceService.batch_replace({'%MY_WORK_DIR': config.data['my_working_directory']}, filepath)
                            break
