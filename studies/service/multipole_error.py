from subprocess import call
from shutil import copyfile

import numpy as np

from studies import config
from studies.entity import Mask
from studies.exceptions import DomainException
from studies.registry import Registry as Reg
from studies.service import ReplaceService, RunService, general


def setup_multipole_mask():
    multipole_errors = config.data['multipole_errors']['status']

    # A. Check if nominal values are used
    if config.data['multipole_errors']['nominal_value']:
        str_block = ''
        if 'b3' in multipole_errors:
            str_block = 'b3a=-2.8e-03;! +/- 5.7e-04 \nb3b=1.6e-03;! +/- 3.2e-04'
        if 'b5' in multipole_errors:
            str_block = str_block+'\n\nb5a=-7.9e+00;! +/- 5.4e-01 \nb5b=-6.8e+00;! +/- 1.5e+00'
        if 'b7' in multipole_errors:
            str_block = str_block+'\n\nb7a=8.8e+04;! +/- 2.6e+04 \nb7b=1.7e+05;! +/- 0.8e+05'
    else:
        Reg.store('scan_multipole_values', np.arange(config.data['multipole_errors']['values_limits']['min'],
                                                      config.data['multipole_errors']['values_limits']['max'],
                                                      config.data['multipole_errors']['values_limits']['step']))
        values_range = Reg.get('scan_multipole_values')
        for value in values_range:
            str_block = ''
            if 'b3' in multipole_errors:
                str_block = 'b3a=(-2.8e-03)*{};! +/- 5.7e-04 \nb3b=(1.6e-03)*{};! +/- 3.2e-04'.format(value,value)
            if 'b5' in multipole_errors:
                str_block = str_block + '\n\nb5a=(-7.9e+00)*{};! +/- 5.4e-01 \nb5b=(-6.8e+00)*{};! +/- 1.5e+00'.format(value,value)
            if 'b7' in multipole_errors:
                str_block = str_block + '\n\nb7a=(8.8e+04)*{};! +/- 2.6e+04 \nb7b=(1.7e+05)*{};! +/- 0.8e+05'.format(value,value)

            ReplaceService.batch_replace({'%MUL_ERRORS': str_block},
                                         './generated/SPStemplate_MV_MULT_D_z_dpp_Qxp_Qyp_optics_deg.mask.temp',
                                         './generated/SPStemplate_MV_MULT_times{}_D_z_dpp_Qxp_Qyp_optics_deg.mask.temp'.format(
                                           value))

    # B. Check if random values are added to the errors
    if config.data['multipole_errors']['random_flag']:
        # firstly save in Reg so you can use later eg in the name
        Reg.store('scan_random_percentage', np.arange(config.data['multipole_errors']['random_limits']['min'],
                                                     config.data['multipole_errors']['random_limits']['max'],
                                                     config.data['multipole_errors']['random_limits']['step']))

        random_range = Reg.get('scan_random_percentage')
        for random in random_range:
            str_block_2 = ''
            if 'b3' in multipole_errors:
                str_block_2 = str_block_2+'\nb3ar=b3a*{};\nb3br=b3b*{};\n'.format(random, random)
            if 'b5' in multipole_errors:
                str_block_2 = str_block_2 + '\nb5ar=b5a*{};\nb5br=b5b*{};\n'.format(random, random)
            if 'b7' in multipole_errors:
                str_block_2 = str_block_2 + '\nb7ar=b7a*{};\nb7br=b7b*{};\n'.format(random, random)

            ReplaceService.batch_replace({'%MUL_ERRORS': str_block+str_block_2},'./generated/SPStemplate_MV_MULT_D_z_dpp_Qxp_Qyp_optics_deg.mask.temp','./generated/SPStemplate_MV_MULT_random{}_D_z_dpp_Qxp_Qyp_optics_deg.mask.temp'.format(random))

    else:
        ReplaceService.batch_replace({'%MUL_ERRORS': str_block},'./generated/SPStemplate_MV_MULT_D_z_dpp_Qxp_Qyp_optics_deg.mask.temp')


def setup():
    general.setup_fort3()
    general.setup_mask()
    general.setup_sixdeskenv()
    setup_multipole_mask()

    Reg.store('mask', Mask.build_from_conf())

    Reg.store('scan_z', np.arange(config.data['z_limits']['min'],
                                  config.data['z_limits']['max'],
                                  config.data['z_limits']['step']))


def get_study_name():
    mask = Reg.get('mask')
    z = Reg.get('z')

    if config.data['multipole_errors']['random_flag']:
        random = Reg.get('random')
        return 'VCC{}_{}_random{}_{}_Z{}_DPP{}_QXP{}_QYP{}_Q{}_EN{}_{}'.format(
            '0.0', config.data['multipole_errors']['status'], random, mask.status_type, str(z), str(mask.dpp),
            str(mask.qxp), str(mask.qyp), mask.optics, mask.energy,
            config.data['type_of_study'])

    if config.data['multipole_errors']['nominal_value']:
        return 'VCC{}_{}_{}_Z{}_DPP{}_QXP{}_QYP{}_Q{}_EN{}_{}'.format(
            '0.0', config.data['multipole_errors']['status'], mask.status_type, str(z), str(mask.dpp),
            str(mask.qxp), str(mask.qyp), mask.optics, mask.energy,
            config.data['type_of_study'])
    else:
        value = Reg.get('value')
        return 'VCC{}_{}_times{}_{}_Z{}_DPP{}_QXP{}_QYP{}_Q{}_EN{}_{}'.format(
            '0.0', config.data['multipole_errors']['status'], value, mask.status_type, str(z), str(mask.dpp),
            str(mask.qxp), str(mask.qyp), mask.optics, mask.energy,
            config.data['type_of_study'])

def run(mask_name):
    common_path = "/scratch0/w1/sixjobs"
    path_to_sixdeskenv = str(config.data['my_working_directory']) + "/" + str(config.workspace) + str(common_path)
    copyfile('{}/sixdeskenv.{}'.format(path_to_sixdeskenv, mask_name),
             '{}/sixdeskenv'.format(path_to_sixdeskenv))
    call(['./run.sh', config.data['my_working_directory'], config.workspace], shell=False)


def scan_z():

    common_path = "/scratch0/w1/sixjobs/mask"
    path_to_mask_direcotry = str(config.data['my_working_directory']) + "/" + str(config.workspace) + str(common_path)

    # A. copy the correct files for the executable. In this def() because scan_z is executed always
    if config.data['multipole_errors']['random_flag']:
        copyfile('./templates/sps_assignRandomMultipoles_upto7_many_values.cmd',
                 path_to_mask_direcotry + '/sps_assignMultipoles_upto7.cmd')
    else:
        copyfile('./templates/sps_assignMultipoles_upto7_one_value.cmd',
                 path_to_mask_direcotry + '/sps_assignMultipoles_upto7.cmd')

    # B. Iteration over z
    z_range = Reg.get('scan_z')
    for z in z_range:
        Reg.store('z', z)
        mask = Reg.get('mask')
        print "ok"
        # ATTENTION! With the current script you cannot hav iteration in both the multipole values and the random factor
        # C. Iteration through the different values of the errors if they exist
        if not config.data['multipole_errors']['nominal_value']:

            values_range = Reg.get('scan_multipole_values')
            for value in values_range:

                Reg.store('value', value)
                study_name = get_study_name()
                Reg.store('mask_name', general.create_mask_file(mask, study_name, value))
                Reg.store('fort3_name', general.create_fort3_file(study_name))

                yield z

                general.setup_mask_sixdeskenv(Reg.get('mask_name'))
                general.setup_fort3mother(mask, z)

                RunService.store_final_mask_name(Reg.get('mask_name'))
                ReplaceService.replace_working_directory()
                print Reg.get('mask_name')
                general.move_files_to_workspace(Reg.get('fort3_name'), Reg.get('mask_name') + '.mask')
                run(Reg.get('mask_name'))

        # D. Iteration through the random factor for the multipole errors. If it exists.
        if config.data['multipole_errors']['random_flag']:
            random_range = Reg.get('scan_random_percentage')
            for random in random_range:
                Reg.store('random',random)
                study_name = get_study_name()
                Reg.store('mask_name', general.create_mask_file(mask, study_name, random))
                Reg.store('fort3_name', general.create_fort3_file(study_name))

                yield z

                general.setup_mask_sixdeskenv(Reg.get('mask_name'))
                general.setup_fort3mother(mask, z)

                RunService.store_final_mask_name(Reg.get('mask_name'))
                ReplaceService.replace_working_directory()

                general.move_files_to_workspace(Reg.get('fort3_name'), Reg.get('mask_name') + '.mask')
                run(Reg.get('mask_name'))

        elif config.data['multipole_errors']['nominal_value']:
            random = None
            study_name = get_study_name()
            Reg.store('mask_name', general.create_mask_file(mask, study_name, random))
            Reg.store('fort3_name', general.create_fort3_file(study_name))

            yield z

            general.setup_mask_sixdeskenv(Reg.get('mask_name'))
            general.setup_fort3mother(mask, z)

            RunService.store_final_mask_name(Reg.get('mask_name'))
            ReplaceService.replace_working_directory()

            general.move_files_to_workspace(Reg.get('fort3_name'), Reg.get('mask_name') + '.mask')
            run(Reg.get('mask_name'))


def do():
    setup()

    for _ in scan_z():
        pass
