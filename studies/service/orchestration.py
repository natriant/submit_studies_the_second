from studies import config

if config.is_power_supply_ripples():
    from studies.service import power_supply_ripples as study
elif config.is_crab_cavities():
    from studies.service import crab_cavity as study
else:
    from studies.service import multipole_error as study


def cleanup():
    from studies.service import RunService
    RunService.clear_final_mask_names_file()


def do():
    cleanup()
    study.do()
