from shutil import copyfile

import numpy as np

from studies import config
from studies.entity import PowerSupplyRipples
from studies.registry import Registry as Reg
from studies.service import ReplaceService, general

# Create an alias of the parent study (parent can be either crab cavities or multipole errors)
if config.is_crab_cavities():
    from studies.service import crab_cavity as study
else:
    from studies.service import multipole_error as study

parent_setup = study.setup
parent_move_files_to_workspace = general.move_files_to_workspace


def setup_mask():
    call_ripples = 'call,file="%MY_WORK_DIR/{}/w1/sixjobs/mask/' \
        .format(config.workspace)

    power_supply_dictionary = {'!%call_ripples_1': call_ripples + 'ripples_quad_definition_and_installation.madx";',
                               '!%call_ripples_2': call_ripples + 'ripples_quad_set_strength_zero.madx";',
                               '!%call_ripples_3': call_ripples + 'ripples_quad_set_different_very_small_strengths.madx";'}

    ReplaceService.batch_replace(power_supply_dictionary,
                                 './generated/SPStemplate_MV_MULT_D_z_dpp_Qxp_Qyp_optics_deg.mask.temp')


def setup_fort3(freq):
    ripple = Reg.store('ripple', PowerSupplyRipples.build_from_config())
    ripple.frequency = freq
    block_name = ''

    if config.data['power_supply_ripples']['status']:
        block_name = 'DYNK\nNOFILE\nFUN RIPP-dmqx1f50l5 COSF_RIPP -{} {} {}\n'.format(
            ripple.amplitude, freq, ripple.phase)
        block_name += 'SET dmqx1f50l5 average_ms RIPP-dmqx1f50l5 1 -1 0\nNEXT'

    ReplaceService.batch_replace({'/%POWER_SUPPLY_RIPPLE': block_name}, './generated/fort.3.local.template.temp')


def get_study_name():
    mask = Reg.get('mask')
    multipole_errors = config.data['multipole_errors']
    z = Reg.get('z')
    cc_voltage = Reg.get('cc_mask').voltage if Reg.contains('cc_mask') else '0.0'
    ripple = Reg.get('ripple')  # type: PowerSupplyRipples

    return 'VCC{}_{}_{}_Z{}_DPP{}_QXP{}_QYP{}_Q{}_EN{}_{}_RIPP{}_{}Hz'.format(
        cc_voltage, multipole_errors, mask.status_type, str(z), str(mask.dpp),
        str(mask.qxp), str(mask.qyp), mask.optics, mask.energy, config.data['type_of_study'],
        ripple.amplitude, ripple.frequency)


def move_files_to_workspace(fort3_filename, mask_filename):
    parent_move_files_to_workspace(fort3_filename, mask_filename)
    common_path = '../{}/scratch0/w1/sixjobs'.format(config.workspace)
    copyfile('./templates/for_power_supply_ripples/ripples_quad_definition_and_installation.madx', '{}/mask/ripples_quad_definition_and_installation.madx'.format(common_path))
    copyfile('./templates/for_power_supply_ripples/ripples_quad_set_different_very_small_strengths.madx', '{}/mask/ripples_quad_set_different_very_small_strengths.madx'.format(common_path))
    copyfile('./templates/for_power_supply_ripples/ripples_quad_set_strength_zero.madx', '{}/mask/ripples_quad_set_strength_zero.madx'.format(common_path))


def scan_freq():
    ripple = PowerSupplyRipples.build_from_config()
    freq_range = np.arange(ripple.frequency_min, ripple.frequency_max, ripple.frequency_step)

    for freq in freq_range:
        setup_mask()
        setup_fort3(freq)

        study.do()

def do():
    study.setup()
    study.get_study_name = get_study_name
    general.move_files_to_workspace = move_files_to_workspace

    scan_freq()

