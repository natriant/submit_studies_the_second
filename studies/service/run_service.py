from subprocess import call
from shutil import copyfile

from studies import config


class RunService(object):

    __final_mask_names_filepath = './generated/final_mask_names.txt'


    def __init__(self, flag):
        self.flag = flag

    def do(self):
        common_path = "/scratch0/w1/sixjobs"
        path_to_sixdeskenv = str(config.data['my_working_directory']) + "/" + str(config.workspace)+str(common_path)

        with open('./generated/final_mask_names.txt') as f:
            for mask_name in f.readlines():
                if not mask_name:
                    continue

                if self.flag == 'r':

                    copyfile('{}/sixdeskenv.{}'.format(path_to_sixdeskenv,mask_name[:-1]),
                             '{}/sixdeskenv'.format(path_to_sixdeskenv))
                    """RUN"""
                    call(['./run2.sh', config.data['my_working_directory'], config.workspace, mask_name], shell=False)
                    #print config.data['my_working_directory']
                elif self.flag == 's':
                    copyfile('{}/sixdeskenv.{}'.format(path_to_sixdeskenv, mask_name[:-1]),
                             '{}/sixdeskenv'.format(path_to_sixdeskenv))
                    # not sure for this
                    """STATUS"""
                    call(['./run3.sh', config.data['my_working_directory'], config.workspace, mask_name], shell=False)

    @staticmethod
    def clear_final_mask_names_file():
        import os
        if os.path.isfile(RunService.__final_mask_names_filepath):
            os.remove(RunService.__final_mask_names_filepath)

    @staticmethod
    def store_final_mask_name(mask_name):
        """Store the name of the final generated mask file.
        In order to run the study, in a later point, using the -r flag"""
        with open(RunService.__final_mask_names_filepath, 'a+') as f:
            f.write(mask_name + '\n')
