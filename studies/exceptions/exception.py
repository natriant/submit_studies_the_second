class DomainException(Exception):
    """Life is not what I expected"""


class NotImplementedException(Exception):
    """The method is not implemented yet"""
