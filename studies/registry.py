class Registry(object):

    _registry = {}

    @staticmethod
    def contains(key):
        return key in Registry._registry

    @staticmethod
    def get(key):
        return Registry._registry[key]

    @staticmethod
    def store(key, obj):
        Registry._registry[key] = obj
        return obj
