#!/bin/bash

if [ $# -lt 2 ]
  then
    echo "No workspace or mask name was given as parameter"
    exit 1
fi

DIR=$1/$2/scratch0/w1/sixjobs

cd $DIR

/afs/cern.ch/project/sixtrack/SixDesk_utilities/dev/utilities/bash/set_env.sh -l -d $2
/afs/cern.ch/project/sixtrack/SixDesk_utilities/dev/utilities/bash/run_status
/afs/cern.ch/project/sixtrack/SixDesk_utilities/dev/utilities/bash/run_six.sh -i -l