option, warn, info;
option, -echo, -warn, -info;

call, file="%MY_WORK_DIR/%CUR_WORK/scratch0/w1/sixjobs/mask/sps.ele";
call, file="%MY_WORK_DIR/%CUR_WORK/scratch0/w1/sixjobs/mask/sps_newCCloc.seq";
call, file="%MY_WORK_DIR/%CUR_WORK/scratch0/w1/sixjobs/mask/lhc_newwp.str";
call, file="%MY_WORK_DIR/%CUR_WORK/scratch0/w1/sixjobs/mask/elements_%VRFMV_%STATUS.str";
call, file="%MY_WORK_DIR/%CUR_WORK/scratch0/w1/sixjobs/mask/sps_assignMultipoles_upto7.cmd";
call, file="%MY_WORK_DIR/%CUR_WORK/scratch0/w1/sixjobs/mask/PTC.madx";


SPS_setchroma_Q20(QPH_setvalue, QPV_setvalue) : macro = {
        option, -info;
        LSDA0=-0.040896;
        LSDB0=-0.063333733;
        LSFA0=0.04516855;
        LSFB0=0.026760516;
        LSFC0=0.04516855;

        logical.LSDAQPH=-0.0090643256;
        logical.LSDBQPH=-0.0140406480;
        logical.LSFAQPH=0.0300341594;
        logical.LSFBQPH=0.0177853536;
        logical.LSFCQPH=0.0300341594;

        logical.LSDAQPV=-0.0272610246;
        logical.LSDBQPV=-0.0422147313;
        logical.LSFAQPV=0.0100859900;
        logical.LSFBQPV=0.0059841919;
        logical.LSFCQPV=0.0100859900;

        ksda:=logical.LSDAQPH*QPH_setvalue+logical.LSDAQPV*QPV_setvalue+LSDA0;
        ksdb:=logical.LSDBQPH*QPH_setvalue+logical.LSDBQPV*QPV_setvalue+LSDB0;
        ksfa:=logical.LSFAQPH*QPH_setvalue+logical.LSFAQPV*QPV_setvalue+LSFA0;
        ksfb:=logical.LSFBQPH*QPH_setvalue+logical.LSFBQPV*QPV_setvalue+LSFB0;
        ksfc:=logical.LSFCQPH*QPH_setvalue+logical.LSFCQPV*QPV_setvalue+LSFC0;

        kLSDA := ksda;
        kLSDB := ksdb;
        kLSFA := ksfa;
        kLSFB := ksfb;
        kLSFC := ksfc;
        option, info;
};

SPS_setchroma_Q26(QPH_setvalue, QPV_setvalue) : macro = {
        option, -info;
        LSDA0=-0.149628261;
        LSDB0=-0.145613183;     
        LSFA0=0.063256459;
        LSFB0=0.121416689;
        LSFC0=0.063256459;

        logical.LSDAQPH=.011283;         
        logical.LSDBQPH=-.040346;
        logical.LSFAQPH=.04135;
        logical.LSFBQPH=.079565;
        logical.LSFCQPH=.04135;
        
        logical.LSDAQPV=-.11422;
        logical.LSDBQPV=-.08606;
        logical.LSFAQPV=.0097365;
        logical.LSFBQPV=.016931;
        logical.LSFCQPV=.0097365;
                
        ksda:=logical.LSDAQPH*QPH_setvalue+logical.LSDAQPV*QPV_setvalue+LSDA0;
        ksdb:=logical.LSDBQPH*QPH_setvalue+logical.LSDBQPV*QPV_setvalue+LSDB0;
        ksfa:=logical.LSFAQPH*QPH_setvalue+logical.LSFAQPV*QPV_setvalue+LSFA0;
        ksfb:=logical.LSFBQPH*QPH_setvalue+logical.LSFBQPV*QPV_setvalue+LSFB0;
        ksfc:=logical.LSFCQPH*QPH_setvalue+logical.LSFCQPV*QPV_setvalue+LSFC0;
        
        kLSDA := ksda;
        kLSDB := ksdb;
        kLSFA := ksfa;
        kLSFB := ksfb;
        kLSFC := ksfc;  
        option, info;
};



fmamarker: marker;
seqedit,sequence=sps;
install,element=ip3..1,class=fmamarker,at=0;!,from=ip3;
install,element=ip3..2,class=fmamarker,at=0;!,from=ip3;
install,element=ip3..3,class=fmamarker,at=0;!,from=ip3;
install,element=ip3..4,class=fmamarker,at=0;!,from=ip3;
install,element=ip3..5,class=fmamarker,at=0;!,from=ip3;
install,element=ip3..6,class=fmamarker,at=0;!,from=ip3;
install,element=ip3..7,class=fmamarker,at=0;!,from=ip3;
install,element=ip3..8,class=fmamarker,at=0;!,from=ip3;
install,element=ip3..9,class=fmamarker,at=0;!,from=ip3;
install,element=ip3..10,class=fmamarker,at=0;!,from=ip3;
endedit;



Beam, particle = proton, energy = 26.0, NPART=1.3E11, BUNCHED;

use, sequence=sps;

!%CC

select, flag=twiss,column=name, s, betx, bety, alfx, alfy, dx, dpx, dy, dpy;
twiss, sequence=sps, file=file.out;
makethin,sequence=sps,style=teapot,makedipedge=true;

use,period=sps;
twiss,sequence=sps,file="sps_thin.tfs"; !necessary to save the thin seq

use, sequence=sps;
!%call_ripples_1
use, sequence=sps;
!%call_ripples_2


!Move the START here so you can calculate easily where to install the new elements
USE, sequence=sps;
seqedit, sequence=sps;
CYCLE, START=IPMYMARKER.START;
Endedit;

use, sequence=sps;
%MUL_ERRORS

QPH = -0.1499; !0.3534615385;
QPV = 0.3874; !0.06769230769;
!*/

exec, AssignMultipoles;

twiss,sequence=sps,file="sps_mult.tfs";

! Match the tune:
match,sequence=sps;
global, q1=%OPTICS.13,q2=%OPTICS.18;
kQF2:=kQF1;
vary, name=kQF1;
vary, name=kQD;
lmdif,calls=10,tolerance=1.0e-21;
endmatch;

twiss;
exec, SPS_setchroma_Q%OPTICS(QPH, QPV);
twiss;

match, sequence=sps;
vary, name=QPH;
vary, name=QPV;
global, dq1=%QXP, dq2=%QYP;
lmdif, calls=1000, tolerance=1e-20;
endmatch;
twiss;


!%call_ripples_3

twiss,sequence=sps,file="sps_corTune.tfs";
SIXTRACK, CAVALL, RADIUS = 17E-03;

stop;
