#!/usr/bin/env python

import getopt
import sys

from studies import config
from studies.exceptions import DomainException
from studies.service import RunService


def usage():
    print """
Version: 28-10-2018
Description: prepare files for sixdesk
Authors: natriant <natalia.triantafyllou  (at) cern.ch>
         vnaskos  <vnaskos (at) gmail.com>
Usage: -w <workspace_name>

Parameters:
    -h               help
    -w --workspace   delay between frames (required)
    -p --prepare     prepare files before run
    -r --run         run the prepared files
    -s --status      run "status" for the prepared files
"""


def main(argv):
    workspace_name = ''
    prepare_flag = False
    run_flag = False
    status_flag = False

    try:
        opts, args = getopt.getopt(argv, "hw:prs", ["help", "workspace=", "prepare", "run", "status"])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-w", "--workspace"):
            workspace_name = arg
        elif opt in ("-p", "--prepare"):
            prepare_flag = True
        elif opt in ("-r", "--run"):
            run_flag = True
        elif opt in ("-s", "--status"):
            status_flag = True

    if not workspace_name:
        usage()
        raise DomainException('No workspace was given')

    if not prepare_flag and not run_flag and not status_flag:
        usage()
        raise DomainException('No action was selected')

    config.init_data()
    config.workspace = workspace_name

    if prepare_flag:
        from studies.service import orchestration
        orchestration.do()

    if run_flag:
        config.init_data()
        run = RunService('r')
        run.do()

    if status_flag:
        run = RunService('s')
        run.do()

    print('Hooooraay!')


if __name__ == "__main__":
    main(sys.argv[1:])
