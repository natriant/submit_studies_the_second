#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "No workspace was given as parameter"
    exit 1
fi

DIR=$1/$2/scratch0/w1/sixjobs

cd $DIR

/afs/cern.ch/project/sixtrack/SixDesk_utilities/dev/utilities/bash/set_env.sh -s -l
/afs/cern.ch/project/sixtrack/SixDesk_utilities/dev/utilities/bash/mad6t.sh -s